export default {
  serverMiddleware: [{ path: '/api', handler: '~/api/index.js' }],
  telemetry: false,
  axios: {
    baseURL:
      process.env.NODE_ENV == 'development'
        ? 'http://localhost:3000/aliexpress-experience/api/'
        : 'https://aliexpressbc.nznvaas.io/aliexpress-experience/api/' // Used as fallback if no runtime config is provided
  },

  router: {
    base: '/aliexpress-experience/'
  },

  publicRuntimeConfig: {
    baseURL: 'https://stg.panel.aliexpress.nznvaas.io',
    aliURL: 'https://vaasservices.nznvaas.io/api/v1/aliexpress/produtos'
  },
  head: {
    title: 'AliExpress Experience - Melhores Descontos, Cupons e mais',
    htmlAttrs: {
      lang: 'pt-br'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'O AliExpress traz até o TecMundo as melhores ofertas. Acesse e confira cupons de descontos e ofertas do maior marketplace do mundo!'
      },
      {
        name: 'og:image',
        content:
          'https://ik.imagekit.io/nznvaas/aliexpress/wp-content/uploads/2021/08/Aliexpress-Experience-logo.png'
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href:
          'https://aliexpressbc.nznvaas.io/aliexpress-experience/tecmundo.ico'
      },
      {
        rel: 'canonical',
        href: 'https://www.tecmundo.com.br/aliexpress-experience'
      }
    ]
  },
  styleResources: {
    scss: ['~/assets/scss/variables.scss']
  },

  css: [
    '~/assets/scss/global.scss',
    '~/assets/css/normalize.css',
    'boxicons/css/boxicons.min.css'
  ],

  plugins: [
    { src: '~/plugins/listeners.js', mode: 'client' },
    { src: '~/plugins/media.js' },
    { src: '~/plugins/carousel.js' },
    { src: '~/plugins/placeholder.js' },
    { src: '~/plugins/product.js' }
  ],

  components: true,

  fontawesome: {
    icons: {
      solid: true,
      brands: true
    }
  },

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    '@nuxtjs/gtm',
    [
      'nuxt-lazy-load',
      {
        defaultImage:
          'https://aliexpressbc.nznvaas.io/aliexpress-experience/loading.gif'
      }
    ]
  ],
  gtm: {
    id: 'GTM-N6JK3VL',
    enabled: true
  },
  build: {
    publicPath:
      process.env == 'development'
        ? ''
        : 'https://aliexpressbc.nznvaas.io/aliexpress-experience/_nuxt'
  },
  buildModules: ['@nuxtjs/google-fonts'],
  googleFonts: {
    preload: true,
    families: {
      Poppins: [100, 300, 400, 700],
      'Open+Sans': [100, 300, 400, 700]
    }
  }
}
