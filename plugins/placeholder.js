import Vue from 'vue'
import VueContentPlaceholders from 'vue-content-placeholders'
import 'vue-content-placeholders/dist/vue-content-placeholders.css'
Vue.use(VueContentPlaceholders)
