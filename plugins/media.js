export default ({ app }, inject) => {
  const getImg = thumbnail => {
    const baseUrl = app.$config.baseURL
    return baseUrl + thumbnail.data.attributes.url
  }

  inject('getImg', getImg)
}
