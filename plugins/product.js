export default ({ app }, inject) => {
  const getProductDiscount = (regularPrice, currentPrice) => {
    const discount = (currentPrice * 100) / regularPrice
    return `${Math.round(discount - 100)}%`
  }

  inject('getProductDiscount', getProductDiscount)
}
