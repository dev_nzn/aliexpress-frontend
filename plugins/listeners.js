export default ({ app }, inject) => {
  const listeners = () => {
    anchorListener()
    headerListener()
    linksListener()
  }

  const headerListener = () => {
    window.addEventListener('scroll', function() {
      let header = document.querySelector('header')
      if (window.pageYOffset > 50) {
        header.classList.add('is-sticky')
      } else {
        header.classList.remove('is-sticky')
      }
    })
  }

  const linksListener = () => {
    let headerLinks = document.querySelectorAll('.c-nav__link')
    const navMenu = document.querySelector('.js-nav__menu')
    for (var i = 0; i < headerLinks.length; i++) {
      headerLinks[i].addEventListener('click', function() {
        navMenu.classList.remove('is-open')
      })
    }
  }

  const anchorListener = () => {
    let anchor = document.querySelectorAll('.js-anchor')

    for (var i = 0; i < anchor.length; i++) {
      anchor[i].addEventListener('click', function(event) {
        const anchorTo = this.getAttribute('anchor-to')
        const element = document.getElementById(anchorTo)

        const topPosition =
          element.getBoundingClientRect().top + window.pageYOffset

        window.scrollTo({
          top: topPosition - 80,
          behavior: 'smooth'
        })
      })
    }
  }

  inject('activeListeners', listeners)
}
