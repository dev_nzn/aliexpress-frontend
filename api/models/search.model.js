import axios from 'axios'
let promises = []
let today = new Date()
  .toJSON()
  .slice(0, 10)
  .replace(/-/g, '-')

export async function searchProduct(term, callBack) {
  promises = []
  try {
    let resultsFromStrapi = await axios.get(
      `https://stg.panel.aliexpress.nznvaas.io/api/products?filters[title][$containsi]=${term}&populate=*&filters[validity][$gt]=${today}`
    )
    let resultsFromAliexpressApi = await axios.get(
      'https://vaasservices.nznvaas.io/testeprodutos'
    )
    formatResultsFromAliexpressApi(resultsFromAliexpressApi.data.product, term)
    Promise.all(promises).then(async formattingResults => {
      const finalResults = await mergeResults(
        resultsFromStrapi.data,
        formattingResults
      )
      callBack(null, finalResults)
    })
  } catch (err) {
    callBack(err, null)
  }
}

export function mergeResults(resultsFromStrapi, resultsFromAliexpressApi) {
  return new Promise((res, rej) => {
    const newArray = resultsFromStrapi.data.concat(resultsFromAliexpressApi)
    res(newArray)
  })
}

export function formatResultsFromAliexpressApi(products, term) {
  products.forEach(async product => {
    const productTitle = product.product_title.toLowerCase()
    if (productTitle.includes(term.toLowerCase())) {
      promises.push(createNewObj(product))
    }
  })
}

export function createNewObj(object) {
  return new Promise((res, rej) => {
    const newObj = {
      attributes: {
        link: object.promotion_link,
        thumbnail: object.product_main_image_url,
        title: object.product_title,
        regular_price: object.original_price,
        current_price: object.app_sale_price
      }
    }
    res(newObj)
  })
}
