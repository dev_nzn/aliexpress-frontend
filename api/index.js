import express from 'express'
import cors from 'cors'
import axios from 'axios'
import { searchProduct } from './models/search.model'

const today = new Date()
  .toJSON()
  .slice(0, 10)
  .replace(/-/g, '-')

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded())

app.get('/category', (req, res) => {
  axios
    .get('https://stg.panel.aliexpress.nznvaas.io/api/categories?populate=*')
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

app.get('/offer', (req, res) => {
  const page = req.query.page
  axios
    .get(
      `https://stg.panel.aliexpress.nznvaas.io/api/products?pagination[page]=${page}&pagination[pageSize]=8&populate=*&sort[0]=publishedAt%3Adesc&filters[hot][$ne]=true&filters[validity][$gt]=${today}`
    )
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

app.get('/highlight', (req, res) => {
  axios
    .get(
      `https://stg.panel.aliexpress.nznvaas.io/api/products?filters[hot][$eq]=true&populate=*&sort[0]=publishedAt%3Adesc&filters[validity][$gt]=${today}`
    )
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

app.get('/search', (req, res) => {
  const term = req.query.term
  searchProduct(term, (err, result) => {
    if (err) {
      res.status(500).json(err)
    } else {
      res.status(202).json({
        success: 1,
        data: result
      })
    }
  })
})

app.get('/product', (req, res) => {
  const category = req.query.category
  axios
    .get(
      `https://vaasservices.nznvaas.io/api/v1/aliexpress/produtos/genericos/${category}`
    )
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

app.get('/coupon', (req, res) => {
  axios
    .get('https://stg.panel.aliexpress.nznvaas.io/api/coupons?populate=*')
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

app.get('/new', (req, res) => {
  const page = req.query.page
  axios
    .get(
      `https://www.tecmundo.com.br/api/v1/tag?top=6&tags=aliexpress&page=${page}`
    )
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

app.get('/faq', (req, res) => {
  axios
    .get('https://stg.panel.aliexpress.nznvaas.io/api/faqs?populate=*')
    .then(result => res.status(202).json(result.data))
    .catch(err => res.status(500).json(err))
})

export default {
  path: '/api',
  handler: app
}
